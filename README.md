# Github Users #

This is the sample application which shows the list of users and their repositories.

Architecture:
MVVM(Android data binding)

Database:
ORMLite

Libraries Used:
Retrofit
RxJava
Dagger2
Glide
Butterknife

Unit Testing:
Espresso