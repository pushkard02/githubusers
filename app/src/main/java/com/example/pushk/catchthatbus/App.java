package com.example.pushk.catchthatbus;

import android.app.Application;
import android.content.Context;

import com.example.pushk.catchthatbus.dagger.component.DaggerNetComponent;
import com.example.pushk.catchthatbus.dagger.component.NetComponent;
import com.example.pushk.catchthatbus.dagger.module.AppModule;
import com.example.pushk.catchthatbus.dagger.module.DBModule;
import com.example.pushk.catchthatbus.dagger.module.NetModule;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by pushk on 04-05-2017.
 */

public class App extends Application {

    private NetComponent mNetComponent;
    private Scheduler scheduler;
    private String BASE_URL = "https://api.github.com/";

    @Override
    public void onCreate() {
        super.onCreate();

        mNetComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(BASE_URL))
                .dBModule(new DBModule(this))
                .build();
    }

    public NetComponent getNetComponent() {
        return mNetComponent;
    }

    private static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    public static App create(Context context) {
        return App.get(context);
    }

    public Scheduler subscribeScheduler() {
        if (scheduler == null) {
            scheduler = Schedulers.io();
        }

        return scheduler;
    }

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

}