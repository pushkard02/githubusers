package com.example.pushk.catchthatbus.api;

import com.example.pushk.catchthatbus.model.User;
import com.example.pushk.catchthatbus.model.UserDetail;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by pushk on 05-05-2017.
 */

public interface PeopleService{

    //fetches the list of users and returns an observable
    @GET("/users")
    Observable<List<User>> fetchPeople();

    //fetches the list of repositories of a user and returns an observable
    @GET("/users/{login}/repos")
    Observable<List<UserDetail>> fetchUserRepos(@Path("login") String login, @Query("page") int page);

}