package com.example.pushk.catchthatbus.dagger.component;

import com.example.pushk.catchthatbus.dagger.module.AppModule;
import com.example.pushk.catchthatbus.dagger.module.DBModule;
import com.example.pushk.catchthatbus.dagger.module.NetModule;
import com.example.pushk.catchthatbus.view.MainActivity;
import com.example.pushk.catchthatbus.view.PeopleDetailActivity;
import com.example.pushk.catchthatbus.viewmodel.PeopleDetailViewModel;
import com.example.pushk.catchthatbus.viewmodel.PeopleViewModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by pushk on 04-05-2017.
 */

@Singleton
@Component(modules = {AppModule.class, NetModule.class, DBModule.class})
public interface NetComponent {
    void inject(PeopleViewModel activity);
    void inject(PeopleDetailViewModel activity);
}
