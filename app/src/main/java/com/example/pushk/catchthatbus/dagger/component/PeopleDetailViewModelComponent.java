package com.example.pushk.catchthatbus.dagger.component;

import com.example.pushk.catchthatbus.dagger.module.PeopleDetailViewModelModule;
import com.example.pushk.catchthatbus.view.PeopleDetailActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by pushk on 07-05-2017.
 */

@Singleton
@Component(modules = {PeopleDetailViewModelModule.class})
public interface PeopleDetailViewModelComponent {
    void inject(PeopleDetailActivity activity);
}
