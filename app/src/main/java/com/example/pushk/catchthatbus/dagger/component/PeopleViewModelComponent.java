package com.example.pushk.catchthatbus.dagger.component;

import com.example.pushk.catchthatbus.dagger.module.PeopleViewModelModule;
import com.example.pushk.catchthatbus.view.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by pushk on 07-05-2017.
 */

@Singleton
@Component(modules = {PeopleViewModelModule.class})
public interface PeopleViewModelComponent {
    void inject(MainActivity activity);
}
