package com.example.pushk.catchthatbus.dagger.module;

import android.app.Application;

import com.example.pushk.catchthatbus.databaseutils.DBHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by pushk on 04-05-2017.
 */

@Module
public class AppModule {
    Application mApplication;

    public AppModule(Application mApplication) {
        this.mApplication = mApplication;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return mApplication;
    }

}
