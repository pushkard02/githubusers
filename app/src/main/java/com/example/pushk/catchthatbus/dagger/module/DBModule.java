package com.example.pushk.catchthatbus.dagger.module;

import android.content.Context;

import com.example.pushk.catchthatbus.databaseutils.DBHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by pushk on 05-05-2017.
 */

@Module
public class DBModule {

    Context mContext;

    public DBModule(Context context){
        this.mContext = context;
    }

    @Provides
    @Singleton
    DBHelper provideDbHelper() {
        return new DBHelper(mContext);
    }
}
