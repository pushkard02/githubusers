package com.example.pushk.catchthatbus.dagger.module;

import android.content.Context;

import com.example.pushk.catchthatbus.model.User;
import com.example.pushk.catchthatbus.viewmodel.PeopleDetailViewModel;

import dagger.Module;
import dagger.Provides;

/**
 * Created by pushk on 07-05-2017.
 */

@Module
public class PeopleDetailViewModelModule {

    Context mContext;
    User user;

    public PeopleDetailViewModelModule(Context context, User user) {
        this.mContext = context;
        this.user = user;
    }

    @Provides
    PeopleDetailViewModel providePeopleViewModel() {
        return new PeopleDetailViewModel(mContext,user);
    }

}
