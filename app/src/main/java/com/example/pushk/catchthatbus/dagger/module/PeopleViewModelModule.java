package com.example.pushk.catchthatbus.dagger.module;

import android.content.Context;

import com.example.pushk.catchthatbus.viewmodel.PeopleViewModel;


import dagger.Module;
import dagger.Provides;

/**
 * Created by pushk on 07-05-2017.
 */

@Module
public class PeopleViewModelModule {

    Context mContext;

    public PeopleViewModelModule(Context context) {
        this.mContext = context;
    }

    @Provides
    PeopleViewModel providePeopleViewModel() {
        return new PeopleViewModel(mContext);
    }
}
