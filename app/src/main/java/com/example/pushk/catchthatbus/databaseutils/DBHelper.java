package com.example.pushk.catchthatbus.databaseutils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.pushk.catchthatbus.model.User;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by pushk on 04-05-2017.
 */

@Singleton
public class DBHelper extends OrmLiteSqliteOpenHelper {

    public static final String DB_NAME = "user_manager.db";
    private static final int DB_VERSION = 1;
    private Dao<User, Integer> userDao;

    @Inject
    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource cs) {
        try {
            TableUtils.createTable(cs, User.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource cs, int oldVersion, int newVersion) {

    }

    //returns the dao for user
    public Dao<User, Integer> getUserDao() throws SQLException {
        if (userDao == null) {
            userDao = getDao(User.class);
        }
        return userDao;
    }

    //returns the list of users from user table
    public List getAllUsers() {

        List mUserList = new ArrayList<>();
        try {
            mUserList.addAll(getUserDao().queryForAll());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mUserList;
    }

    //save the user data in user table
    public void saveUser(User userData){
        try {
            getUserDao().createOrUpdate(userData);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //saves the list of users in the user table
    public void saveListOfUsers(List<User> userList){
        try {
            for(User user : userList) {
                getUserDao().createOrUpdate(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}