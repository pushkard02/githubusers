package com.example.pushk.catchthatbus.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pushk on 05-05-2017.
 */

public class Owner {

    @SerializedName("type")
    private String type;

    @SerializedName("login")
    private String login;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
