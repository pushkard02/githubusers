package com.example.pushk.catchthatbus.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by pushk on 04-05-2017.
 */

@DatabaseTable(tableName = "users")
public class User implements Serializable{
    @SerializedName("id")
    @DatabaseField(columnName = "id",generatedId = true)
    private int id;

    @SerializedName("login")
    @DatabaseField(columnName = "login")
    private String login;

    @SerializedName("avatar_url")
    @DatabaseField(columnName = "avatar_url")
    private String avatarUrl;


    // Getter and Setter method of fields

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}