package com.example.pushk.catchthatbus.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by pushk on 05-05-2017.
 */

public class UserDetail implements Serializable{

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("owner")
    public Owner owner;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}
