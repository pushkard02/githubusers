package com.example.pushk.catchthatbus.view;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.pushk.catchthatbus.R;
import com.example.pushk.catchthatbus.dagger.component.DaggerPeopleViewModelComponent;
import com.example.pushk.catchthatbus.dagger.component.PeopleViewModelComponent;
import com.example.pushk.catchthatbus.dagger.module.PeopleViewModelModule;
import com.example.pushk.catchthatbus.databinding.PeopleActivityBinding;
import com.example.pushk.catchthatbus.viewmodel.PeopleViewModel;

import java.util.Observable;
import java.util.Observer;

import javax.inject.Inject;

/**
 * Created by pushk on 05-05-2017.
 */

public class MainActivity extends AppCompatActivity implements Observer {

    private static final String TAG = MainActivity.class.getSimpleName();
    //SPAN_COUNT is the number of views which needs to be displayed in a row in recycler view
    private static final int SPAN_COUNT = 2;

    @Inject
    PeopleViewModel userViewModel;

    private PeopleActivityBinding peopleActivityBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Injecting PeopleViewModel
        PeopleViewModelComponent peopleViewModelComponent = DaggerPeopleViewModelComponent.builder()
                .peopleViewModelModule(new PeopleViewModelModule(this)).build();
        peopleViewModelComponent.inject(this);
        //data binding with view model
        initDataBinding();
        //setting up recyclerview
        setupListUserView(peopleActivityBinding.listPeople);
        //set up observer
        setupObserver(userViewModel);
    }

    //initialize the data binding
    private void initDataBinding() {
        peopleActivityBinding = DataBindingUtil.setContentView(this, R.layout.people_activity);
        peopleActivityBinding.setMainViewModel(userViewModel);
    }

    //initialize the recycler view and set the adapter
    private void setupListUserView(RecyclerView userRecycler) {
        UserListAdapter adapter = new UserListAdapter(this, userViewModel.getUserList());
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, SPAN_COUNT);
        userRecycler.setLayoutManager(mLayoutManager);
        userRecycler.setItemAnimator(new DefaultItemAnimator());
        userRecycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        userRecycler.setAdapter(adapter);
    }

    //sets up the observer
    public void setupObserver(Observable observable) {
        observable.addObserver(this);
    }

    //this is the callback which we get when data has been successfully fetched by the retrofit API
    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof PeopleViewModel) {
            UserListAdapter peopleAdapter = (UserListAdapter) peopleActivityBinding.listPeople.getAdapter();
            PeopleViewModel peopleViewModel = (PeopleViewModel) o;
            peopleAdapter.setPeopleList(peopleViewModel.getUserList());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //we are resetting PeopleViewModel when activity is about to destroy
        userViewModel.reset();
    }
}
