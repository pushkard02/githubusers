package com.example.pushk.catchthatbus.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.pushk.catchthatbus.R;
import com.example.pushk.catchthatbus.dagger.component.DaggerPeopleDetailViewModelComponent;
import com.example.pushk.catchthatbus.dagger.component.PeopleDetailViewModelComponent;
import com.example.pushk.catchthatbus.dagger.module.PeopleDetailViewModelModule;
import com.example.pushk.catchthatbus.databinding.PeopleDetailActivityBinding;
import com.example.pushk.catchthatbus.model.User;
import com.example.pushk.catchthatbus.viewmodel.PeopleDetailViewModel;

import java.util.Observable;
import java.util.Observer;

import javax.inject.Inject;

/**
 * Created by pushk on 05-05-2017.
 */

public class PeopleDetailActivity extends AppCompatActivity implements Observer{

    public static final String EXTRA_PEOPLE = "EXTRA_PEOPLE";

    @Inject
    PeopleDetailViewModel userDetailViewModel;

    private PeopleDetailActivityBinding peopleDetailActivityBinding;
    private User people;
    private RecyclerViewScrollListener recyclerViewScrollListener;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getExtrasFromIntent();
        //Injecting PeopleDetailViewModel
        PeopleDetailViewModelComponent peopleViewModelComponent = DaggerPeopleDetailViewModelComponent.builder()
                .peopleDetailViewModelModule(new PeopleDetailViewModelModule(this,people)).build();
        peopleViewModelComponent.inject(this);
        //data binding with view model
        initDataBinding();
        //setting up tool bar
        setSupportActionBar(peopleDetailActivityBinding.toolbar);
        //display home button
        displayHomeAsUpEnabled();
        //set up recycler view for displaying repo list
        setupUserDetailView(peopleDetailActivityBinding.listPeople);
        //set up observer
        setupObserver(userDetailViewModel);
    }

    //initialize the data binding
    private void initDataBinding() {
        peopleDetailActivityBinding =
                DataBindingUtil.setContentView(this, R.layout.people_detail_activity);
        peopleDetailActivityBinding.setPeopleDetailViewModel(userDetailViewModel);
    }

    //initialize the recycler view and set the adapter
    private void setupUserDetailView(RecyclerView userRecycler) {
        RepoListAdapter adapter = new RepoListAdapter(this, userDetailViewModel.getRepoList());
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        userRecycler.setLayoutManager(mLayoutManager);
        userRecycler.setItemAnimator(new DefaultItemAnimator());
        userRecycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        userRecycler.setAdapter(adapter);
        recyclerViewScrollListener = new RecyclerViewScrollListener(mLayoutManager){
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                loadNextDataFromApi(page);
            }
        };
        userRecycler.addOnScrollListener(recyclerViewScrollListener);
    }

    //Loads more data by calling the api through retrofit
    private void loadNextDataFromApi(int page) {
        userDetailViewModel.fetchUserDetails(page);
    }

    //set up the observer
    public void setupObserver(Observable observable) {
        observable.addObserver(this);
    }

    public static Intent launchDetail(Context context, User people) {
        Intent intent = new Intent(context, PeopleDetailActivity.class);
        intent.putExtra(EXTRA_PEOPLE, people);
        return intent;
    }

    private void displayHomeAsUpEnabled() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    //get extras from intent
    private void getExtrasFromIntent() {
        people = (User) getIntent().getSerializableExtra(EXTRA_PEOPLE);
        setTitle(people.getLogin());
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof PeopleDetailViewModel) {
            RepoListAdapter peopleAdapter = (RepoListAdapter) peopleDetailActivityBinding.listPeople.getAdapter();
            PeopleDetailViewModel peopleViewModel = (PeopleDetailViewModel) o;
            peopleAdapter.setRepoList(peopleViewModel.getRepoList(),peopleViewModel.previousItemCount,peopleViewModel.newItemFetchedCount);
        }
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        userDetailViewModel.reset();
        recyclerViewScrollListener.resetState();
    }
}
