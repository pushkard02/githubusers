package com.example.pushk.catchthatbus.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.pushk.catchthatbus.R;
import com.example.pushk.catchthatbus.databinding.UserCardBinding;
import com.example.pushk.catchthatbus.databinding.UserDetailsCardBinding;
import com.example.pushk.catchthatbus.model.User;
import com.example.pushk.catchthatbus.model.UserDetail;
import com.example.pushk.catchthatbus.viewmodel.ItemPeopleViewModel;
import com.example.pushk.catchthatbus.viewmodel.UserDetailPeopleViewModel;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by pushk on 05-05-2017.
 */

public class RepoListAdapter extends RecyclerView.Adapter<RepoListAdapter.MyViewHolder> {

    private Context mContext;
    private List<UserDetail> repoList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        UserDetailsCardBinding mUserDetailPeopleBinding;

        public MyViewHolder(UserDetailsCardBinding itemPeopleBinding) {
            super(itemPeopleBinding.userDetailLayout);
            this.mUserDetailPeopleBinding = itemPeopleBinding;
        }

        void bindPeople(UserDetail people) {
            if (mUserDetailPeopleBinding.getUserDetailPeopleViewModel() == null) {
                mUserDetailPeopleBinding.setUserDetailPeopleViewModel(new UserDetailPeopleViewModel(people,itemView.getContext()));
            } else {
                mUserDetailPeopleBinding.getUserDetailPeopleViewModel().setPeople(people);
            }
        }
    }

    public RepoListAdapter(Context mContext, List<UserDetail> repoList) {
        this.mContext = mContext;
        this.repoList = repoList;
    }

    @Override
    public RepoListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        UserDetailsCardBinding itemView = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.user_details_card,
                parent, false);

        return new RepoListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RepoListAdapter.MyViewHolder holder, int position) {
        holder.bindPeople(repoList.get(position));
    }

    @Override
    public int getItemCount() {
        return repoList.size();
    }

    public void setRepoList(List<UserDetail> repoList,int position, int itemCount) {
        this.repoList = repoList;
        notifyItemRangeInserted(position,itemCount);
    }
}