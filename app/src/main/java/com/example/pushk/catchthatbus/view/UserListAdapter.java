package com.example.pushk.catchthatbus.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.pushk.catchthatbus.R;
import com.example.pushk.catchthatbus.databinding.UserCardBinding;
import com.example.pushk.catchthatbus.model.User;
import com.example.pushk.catchthatbus.viewmodel.ItemPeopleViewModel;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by pushk on 04-05-2017.
 */

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.MyViewHolder> {

    private Context mContext;
    private List<User> userList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        UserCardBinding mItemPeopleBinding;

        public MyViewHolder(UserCardBinding itemPeopleBinding) {
            super(itemPeopleBinding.cardLayout);
            this.mItemPeopleBinding = itemPeopleBinding;
        }

        void bindPeople(User people) {
            if (mItemPeopleBinding.getPeopleViewModel() == null) {
                mItemPeopleBinding.setPeopleViewModel(
                        new ItemPeopleViewModel(people, itemView.getContext()));
            } else {
                mItemPeopleBinding.getPeopleViewModel().setPeople(people);
            }
        }
    }

    public UserListAdapter(Context mContext, List<User> userList) {
        this.mContext = mContext;
        this.userList = userList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        UserCardBinding itemView = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.user_card,
                parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        holder.bindPeople(userList.get(position));
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public void setPeopleList(List<User> peopleList) {
        this.userList = peopleList;
        notifyDataSetChanged();
    }
}