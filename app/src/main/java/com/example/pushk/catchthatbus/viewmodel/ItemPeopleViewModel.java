package com.example.pushk.catchthatbus.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.pushk.catchthatbus.model.User;
import com.example.pushk.catchthatbus.view.PeopleDetailActivity;


/**
 * Created by pushk on 06-05-2017.
 */

public class ItemPeopleViewModel extends BaseObservable {

  private User people;
  private Context context;

  public ItemPeopleViewModel(User people, Context context) {
    this.people = people;
    this.context = context;
  }

  public String getLogin() {
    return people.getLogin();
  }

  public String getPictureProfile() {
    return people.getAvatarUrl();
  }

  @BindingAdapter("imageUrl") public static void setImageUrl(ImageView imageView, String url) {
    Glide.with(imageView.getContext()).load(url).into(imageView);
  }

  public void onItemClick(View view) {
    context.startActivity(PeopleDetailActivity.launchDetail(view.getContext(), people));
  }

  public void setPeople(User people) {
    this.people = people;
    notifyChange();
  }
}
