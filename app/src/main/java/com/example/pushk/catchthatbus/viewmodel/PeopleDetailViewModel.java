package com.example.pushk.catchthatbus.viewmodel;

import android.app.Application;
import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.view.View;

import com.example.pushk.catchthatbus.App;
import com.example.pushk.catchthatbus.R;
import com.example.pushk.catchthatbus.api.PeopleService;
import com.example.pushk.catchthatbus.model.User;
import com.example.pushk.catchthatbus.model.UserDetail;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import retrofit2.Retrofit;

/**
 * Created by pushk on 05-05-2017.
 */

public class PeopleDetailViewModel extends Observable{

    private static final String TAG = PeopleDetailViewModel.class.getSimpleName();
    private static final int INITIAL_PAGE = 1;

    @Inject
    Retrofit retrofit;

    @Inject
    Application application;

    private User user;

    public ObservableInt peopleProgress;
    public ObservableInt peopleRecycler;
    public ObservableInt peopleLabel;
    public ObservableField<String> messageLabel;
    public ObservableInt retryButton;

    private List<UserDetail> repoList;
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    public  int previousItemCount;
    public int newItemFetchedCount;

    public PeopleDetailViewModel(@NonNull Context context, User user) {
        this.user = user;

        this.context = context;
        App.create(context).getNetComponent().inject(PeopleDetailViewModel.this);

        this.repoList = new ArrayList<>();
        peopleProgress = new ObservableInt(View.GONE);
        peopleRecycler = new ObservableInt(View.GONE);
        peopleLabel = new ObservableInt(View.VISIBLE);
        messageLabel = new ObservableField<>(context.getString(R.string.default_loading_people));
        retryButton = new ObservableInt(View.GONE);

        initializeViews();
        fetchUserDetails(INITIAL_PAGE);
    }

    //returns the login name of a user
    public String getLogin() {
        return user.getLogin();
    }

    //returns the avatarUrl of a user
    public String getPictureProfile() {
        return user.getAvatarUrl();
    }

    //initialize views in the repo list activity
    private void initializeViews() {
        peopleLabel.set(View.GONE);
        peopleRecycler.set(View.GONE);
        retryButton.set(View.GONE);
        peopleProgress.set(View.VISIBLE);
    }

    //makes the network request using retrofit and fetches the repo list of a user
    //after success sends the callbacks to the subscribers
    public void fetchUserDetails(int page) {

        Disposable disposable = retrofit.create(PeopleService.class).fetchUserRepos(user.getLogin(),page)
                .subscribeOn(((App) application).subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<UserDetail>>() {
                    @Override
                    public void accept(List<UserDetail> peopleResponse) throws Exception {
                        changePeopleDataSet(peopleResponse);
                        hideUnhideViewsOnSuccess();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        handleUiElementsOnFailure();
                    }
                });

        compositeDisposable.add(disposable);
    }

    //handles views if network request fails
    private void handleUiElementsOnFailure() {
        messageLabel.set(context.getString(R.string.error_loading_users));
        peopleProgress.set(View.GONE);
        peopleLabel.set(View.VISIBLE);
        peopleRecycler.set(View.GONE);
        retryButton.set(View.VISIBLE);
    }

    //hides/unhides views on succesfull api call
    private void hideUnhideViewsOnSuccess() {
        peopleProgress.set(View.GONE);
        peopleLabel.set(View.GONE);
        peopleRecycler.set(View.VISIBLE);
        retryButton.set(View.GONE);
    }

    //updates the previousItemcount and newItemFetchedCount
    //adds the new repos into the existing repo list and notifies the observers
    private void changePeopleDataSet(List<UserDetail> repos) {
        previousItemCount = repoList.size();
        newItemFetchedCount = repos.size();
        repoList.addAll(repos);
        setChanged();
        notifyObservers();
    }

    //returns all the repos of a user
    public List<UserDetail> getRepoList() {
        return repoList;
    }

    //Unsubscribes the observable object
    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    //Resets the view model
    //can be used to reset the view model eg. when the activity is about to destroy
    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }

    //Below method is called when retry button is clicked on repo list page
    // if internet connection is not available
    public void onItemClick(View view) {
        if (repoList.size() <=0) {
            initializeViews();
            fetchUserDetails(INITIAL_PAGE);
        }
    }
}

