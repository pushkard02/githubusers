package com.example.pushk.catchthatbus.viewmodel;

import android.app.Application;
import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import com.example.pushk.catchthatbus.App;
import com.example.pushk.catchthatbus.R;
import com.example.pushk.catchthatbus.api.PeopleService;
import com.example.pushk.catchthatbus.databaseutils.DBHelper;
import com.example.pushk.catchthatbus.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import retrofit2.Retrofit;

/**
 * Created by pushk on 05-05-2017.
 */
public class PeopleViewModel extends Observable {

    private static final String TAG = PeopleViewModel.class.getSimpleName();

    @Inject
    Retrofit retrofit;

    @Inject
    Application application;

    @Inject
    DBHelper dbHelper;

    public ObservableInt peopleProgress;
    public ObservableInt peopleRecycler;
    public ObservableInt peopleLabel;
    public ObservableField<String> messageLabel;
    public ObservableInt retryButton;

    private List<User> peopleList;
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public PeopleViewModel(@NonNull Context context) {

        this.context = context;
        App.create(context).getNetComponent().inject(PeopleViewModel.this);

        this.peopleList = new ArrayList<>();
        peopleProgress = new ObservableInt(View.GONE);
        peopleRecycler = new ObservableInt(View.GONE);
        retryButton = new ObservableInt(View.GONE);
        peopleLabel = new ObservableInt(View.VISIBLE);
        messageLabel = new ObservableField<>(context.getString(R.string.default_loading_people));

        initializeViews();
        checkAndFetchUserList();
    }

    //checks if users are present in database then no need to make network request to fetch the users
    //if not then fetch user list
    private void checkAndFetchUserList() {
        List peopleListFromDb = dbHelper.getAllUsers();
        if (peopleListFromDb.size() > 0 && peopleList.size() <= 0) {
            Log.d(TAG, "Users Data already present");
            changePeopleDataSet(peopleListFromDb);
            hideUnhideViewsOnSuccess();
        } else {
            fetchUserList();
        }
    }

    //initialize views
    public void initializeViews() {
        peopleLabel.set(View.GONE);
        peopleRecycler.set(View.GONE);
        retryButton.set(View.GONE);
        peopleProgress.set(View.VISIBLE);
    }

    //makes the network request using retrofit and fetches the users list
    //after success sends the callbacks to the subscribers
    private void fetchUserList() {

        Disposable disposable = retrofit.create(PeopleService.class).fetchPeople()
                .subscribeOn(((App) application).subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<User>>() {
                    @Override
                    public void accept(List<User> peopleResponse) throws Exception {
                        changePeopleDataSet(peopleResponse);
                        addUsersToDB(peopleResponse);
                        hideUnhideViewsOnSuccess();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        handleUiElementsOnFailure();
                    }
                });

        compositeDisposable.add(disposable);
    }

    //handles views if network request fails
    private void handleUiElementsOnFailure() {
        messageLabel.set(context.getString(R.string.error_loading_users));
        peopleProgress.set(View.GONE);
        peopleLabel.set(View.VISIBLE);
        peopleRecycler.set(View.GONE);
        retryButton.set(View.VISIBLE);
    }

    //hides/unhides views on succesfull api call
    private void hideUnhideViewsOnSuccess() {
        peopleProgress.set(View.GONE);
        peopleLabel.set(View.GONE);
        retryButton.set(View.GONE);
        peopleRecycler.set(View.VISIBLE);
    }

    //inserts users in database
    private void addUsersToDB(List<User> peoples) {
        dbHelper.saveListOfUsers(peoples);
    }

    //adds the new users into the existing list and notifies the observers
    private void changePeopleDataSet(List<User> peoples) {
        peopleList.addAll(peoples);
        setChanged();
        notifyObservers();
    }

    //returns all the users
    public List<User> getUserList() {
        return peopleList;
    }

    //Unsubscribes the observable object
    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    //Resets the view model
    //can be used to reset the view model eg. when the activity is about to destroy
    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }

    //Below method is called when retry button is clicked on user list page
    // if internet connection is not available
    public void onItemClick(View view) {
        if (peopleList.size() <=0) {
            initializeViews();
            fetchUserList();
        }
    }

}
