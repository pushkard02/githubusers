package com.example.pushk.catchthatbus.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.pushk.catchthatbus.model.User;
import com.example.pushk.catchthatbus.model.UserDetail;
import com.example.pushk.catchthatbus.view.PeopleDetailActivity;

/**
 * Created by pushk on 06-05-2017.
 */

public class UserDetailPeopleViewModel extends BaseObservable {

    private UserDetail people;
    private Context context;

    public UserDetailPeopleViewModel(UserDetail people, Context context) {
        this.people = people;
        this.context = context;
    }

    public String getName() {
        return people.getName();
    }

    public String getOwnerType() {
        return people.getOwner().getType();
    }


    @BindingAdapter("imageUrl") public static void setImageUrl(ImageView imageView, String url) {
        Glide.with(imageView.getContext()).load(url).into(imageView);
    }

    public void setPeople(UserDetail people) {
        this.people = people;
        notifyChange();
    }
}
